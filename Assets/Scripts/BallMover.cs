using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMover : MonoBehaviour
{
    public Rigidbody2D BallRB;
    public float BallSpeed = 500;
    // Start is called before the first frame update
    private void Awake()
    {
        BallRB = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        Invoke(nameof(SetRandomTrajectory), 1f);
    }

    void SetRandomTrajectory()
    {
        
        Vector2 Force = Vector2.zero; 
        Force.x = Random.Range(-1f, 1f);
        Force.y = -1f;
        
        BallRB.AddForce(Force.normalized * BallSpeed);
    }
    
}
