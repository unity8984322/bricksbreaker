using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bricks : MonoBehaviour
{
    int health;
    SpriteRenderer brickSprite;
    public Sprite[] spriteStates;
    bool unbrekable;

    private void Awake()
    {
        brickSprite = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!unbrekable)
        {
            health = spriteStates.Length;
            brickSprite.sprite = spriteStates[health-1];
        }
    }

    void BrickHit()
    {
        if (unbrekable)
        {
            Debug.Log("Brick is unbrekable");
            return;
        }
            health--;
        if (health <= 0)
        {
            Debug.Log("health is <= 0");
            gameObject.SetActive(false);
        }
        else
        {
            Debug.Log("Health is not <= 0");
            brickSprite.sprite = spriteStates[health - 1];
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            Debug.Log("Calling BrickHit()");
            BrickHit();
        }
    }
}   
