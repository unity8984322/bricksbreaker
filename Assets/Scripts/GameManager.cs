using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int CurrentLevel = 1;
    public int score = 0;
    public int lives = 3;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        NewGame();
    }

    void NewGame()
    {
        this.score = 0;
        this.lives = 3;
        LoadLevel(1);
    }

    void LoadLevel(int levelnumber)
    {
        this.CurrentLevel = levelnumber;
        SceneManager.LoadScene("Level" + levelnumber);
    }
}

