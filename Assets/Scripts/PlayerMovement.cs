using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D PlayerRB;
    Vector2 Direction;
    public float Speed = 30;

    public float maxBounceAngle=60f;
    
    private void Awake()
    {
        this.PlayerRB = GetComponent<Rigidbody2D>();
    }
    private void Update()   
    {
        if (Input.GetKey(KeyCode.A))
        {
            Direction = Vector2.left;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Direction = Vector3.right;
        }
        else
        {
            Direction = Vector3.zero;
        }    
    }

    private void FixedUpdate()
    {
        if (Direction != Vector2.zero)
        {
            PlayerRB.AddForce(Direction * Speed)  ;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        BallMover ball = collision.gameObject.GetComponent<BallMover>();
        if(ball != null)
        {
            Vector3 PlayerPosition = transform.position;
            Vector3 contactPoint = collision.GetContact(0).point;

            float offset = PlayerPosition.x - contactPoint.x;
            float width = collision.otherCollider.bounds.size.x / 2;
            float Currentangle = Vector2.SignedAngle(Vector2.up, ball.GetComponent<Rigidbody2D>().velocity);
            float bounceAngle = (offset / width) * maxBounceAngle;

            float newAngle = Mathf.Clamp((Currentangle + bounceAngle), -maxBounceAngle, maxBounceAngle);
            Quaternion rotation = Quaternion.AngleAxis(newAngle,Vector3.forward);

            ball.gameObject.GetComponent<Rigidbody2D>().velocity = rotation * Vector3.up * ball.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude;
        }
    }

}
